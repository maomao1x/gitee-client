# WebhooksApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteV5ReposOwnerRepoHooksId**](WebhooksApi.md#deleteV5ReposOwnerRepoHooksId) | **DELETE** v5/repos/{owner}/{repo}/hooks/{id} | 删除一个项目WebHook
[**getV5ReposOwnerRepoHooks**](WebhooksApi.md#getV5ReposOwnerRepoHooks) | **GET** v5/repos/{owner}/{repo}/hooks | 列出项目的WebHooks
[**getV5ReposOwnerRepoHooksId**](WebhooksApi.md#getV5ReposOwnerRepoHooksId) | **GET** v5/repos/{owner}/{repo}/hooks/{id} | 获取项目单个WebHook
[**patchV5ReposOwnerRepoHooksId**](WebhooksApi.md#patchV5ReposOwnerRepoHooksId) | **PATCH** v5/repos/{owner}/{repo}/hooks/{id} | 更新一个项目WebHook
[**postV5ReposOwnerRepoHooks**](WebhooksApi.md#postV5ReposOwnerRepoHooks) | **POST** v5/repos/{owner}/{repo}/hooks | 创建一个项目WebHook
[**postV5ReposOwnerRepoHooksIdTests**](WebhooksApi.md#postV5ReposOwnerRepoHooksIdTests) | **POST** v5/repos/{owner}/{repo}/hooks/{id}/tests | 测试WebHook是否发送成功


<a name="deleteV5ReposOwnerRepoHooksId"></a>
# **deleteV5ReposOwnerRepoHooksId**
> Void deleteV5ReposOwnerRepoHooksId(owner, repo, id, accessToken)

删除一个项目WebHook

删除一个项目WebHook

### Example
```java
// Import classes:
//import com.gitee.api.api.WebhooksApi;

WebhooksApi apiInstance =  new ApiClient().create(WebhooksApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer id = 56; // Integer | Webhook的ID
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.deleteV5ReposOwnerRepoHooksId(owner, repo, id, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **id** | **Integer**| Webhook的ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoHooks"></a>
# **getV5ReposOwnerRepoHooks**
> java.util.List&lt;Hook&gt; getV5ReposOwnerRepoHooks(owner, repo, accessToken, page, perPage)

列出项目的WebHooks

列出项目的WebHooks

### Example
```java
// Import classes:
//import com.gitee.api.api.WebhooksApi;

WebhooksApi apiInstance =  new ApiClient().create(WebhooksApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<Hook>> result = apiInstance.getV5ReposOwnerRepoHooks(owner, repo, accessToken, page, perPage);
result.subscribe(new Observer<java.util.List<Hook>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<Hook> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;Hook&gt;**](Hook.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoHooksId"></a>
# **getV5ReposOwnerRepoHooksId**
> Hook getV5ReposOwnerRepoHooksId(owner, repo, id, accessToken)

获取项目单个WebHook

获取项目单个WebHook

### Example
```java
// Import classes:
//import com.gitee.api.api.WebhooksApi;

WebhooksApi apiInstance =  new ApiClient().create(WebhooksApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer id = 56; // Integer | Webhook的ID
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Hook> result = apiInstance.getV5ReposOwnerRepoHooksId(owner, repo, id, accessToken);
result.subscribe(new Observer<Hook>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Hook response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **id** | **Integer**| Webhook的ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Hook**](Hook.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="patchV5ReposOwnerRepoHooksId"></a>
# **patchV5ReposOwnerRepoHooksId**
> Hook patchV5ReposOwnerRepoHooksId(owner, repo, id, url, accessToken, password, pushEvents, tagPushEvents, issuesEvents, noteEvents, mergeRequestsEvents)

更新一个项目WebHook

更新一个项目WebHook

### Example
```java
// Import classes:
//import com.gitee.api.api.WebhooksApi;

WebhooksApi apiInstance =  new ApiClient().create(WebhooksApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer id = 56; // Integer | Webhook的ID
String url = "url_example"; // String | 远程HTTP URL
String accessToken = "accessToken_example"; // String | 用户授权码
String password = "password_example"; // String | 请求URL时会带上该密码，防止URL被恶意请求
Boolean pushEvents = true; // Boolean | Push代码到仓库
Boolean tagPushEvents = true; // Boolean | 提交Tag到仓库
Boolean issuesEvents = true; // Boolean | 创建/关闭Issue
Boolean noteEvents = true; // Boolean | 评论了Issue/代码等等
Boolean mergeRequestsEvents = true; // Boolean | 合并请求和合并后
Observable<Hook> result = apiInstance.patchV5ReposOwnerRepoHooksId(owner, repo, id, url, accessToken, password, pushEvents, tagPushEvents, issuesEvents, noteEvents, mergeRequestsEvents);
result.subscribe(new Observer<Hook>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Hook response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **id** | **Integer**| Webhook的ID |
 **url** | **String**| 远程HTTP URL |
 **accessToken** | **String**| 用户授权码 | [optional]
 **password** | **String**| 请求URL时会带上该密码，防止URL被恶意请求 | [optional]
 **pushEvents** | **Boolean**| Push代码到仓库 | [optional] [default to true]
 **tagPushEvents** | **Boolean**| 提交Tag到仓库 | [optional]
 **issuesEvents** | **Boolean**| 创建/关闭Issue | [optional]
 **noteEvents** | **Boolean**| 评论了Issue/代码等等 | [optional]
 **mergeRequestsEvents** | **Boolean**| 合并请求和合并后 | [optional]

### Return type

[**Hook**](Hook.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoHooks"></a>
# **postV5ReposOwnerRepoHooks**
> Hook postV5ReposOwnerRepoHooks(owner, repo, url, accessToken, password, pushEvents, tagPushEvents, issuesEvents, noteEvents, mergeRequestsEvents)

创建一个项目WebHook

创建一个项目WebHook

### Example
```java
// Import classes:
//import com.gitee.api.api.WebhooksApi;

WebhooksApi apiInstance =  new ApiClient().create(WebhooksApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String url = "url_example"; // String | 远程HTTP URL
String accessToken = "accessToken_example"; // String | 用户授权码
String password = "password_example"; // String | 请求URL时会带上该密码，防止URL被恶意请求
Boolean pushEvents = true; // Boolean | Push代码到仓库
Boolean tagPushEvents = true; // Boolean | 提交Tag到仓库
Boolean issuesEvents = true; // Boolean | 创建/关闭Issue
Boolean noteEvents = true; // Boolean | 评论了Issue/代码等等
Boolean mergeRequestsEvents = true; // Boolean | 合并请求和合并后
Observable<Hook> result = apiInstance.postV5ReposOwnerRepoHooks(owner, repo, url, accessToken, password, pushEvents, tagPushEvents, issuesEvents, noteEvents, mergeRequestsEvents);
result.subscribe(new Observer<Hook>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Hook response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **url** | **String**| 远程HTTP URL |
 **accessToken** | **String**| 用户授权码 | [optional]
 **password** | **String**| 请求URL时会带上该密码，防止URL被恶意请求 | [optional]
 **pushEvents** | **Boolean**| Push代码到仓库 | [optional] [default to true]
 **tagPushEvents** | **Boolean**| 提交Tag到仓库 | [optional]
 **issuesEvents** | **Boolean**| 创建/关闭Issue | [optional]
 **noteEvents** | **Boolean**| 评论了Issue/代码等等 | [optional]
 **mergeRequestsEvents** | **Boolean**| 合并请求和合并后 | [optional]

### Return type

[**Hook**](Hook.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoHooksIdTests"></a>
# **postV5ReposOwnerRepoHooksIdTests**
> Void postV5ReposOwnerRepoHooksIdTests(owner, repo, id, accessToken)

测试WebHook是否发送成功

测试WebHook是否发送成功

### Example
```java
// Import classes:
//import com.gitee.api.api.WebhooksApi;

WebhooksApi apiInstance =  new ApiClient().create(WebhooksApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer id = 56; // Integer | Webhook的ID
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.postV5ReposOwnerRepoHooksIdTests(owner, repo, id, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **id** | **Integer**| Webhook的ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

