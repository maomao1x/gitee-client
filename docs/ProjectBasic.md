
# ProjectBasic

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**fullName** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**path** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**owner** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**_private** | **String** |  |  [optional]
**fork** | **String** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]



