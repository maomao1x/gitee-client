
# GroupMember

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** |  |  [optional]
**active** | **String** |  |  [optional]
**remark** | **String** |  |  [optional]
**role** | **String** |  |  [optional]
**organizationUrl** | **String** |  |  [optional]
**organization** | [**Group**](Group.md) |  |  [optional]
**user** | **String** |  |  [optional]



