
# UserEmail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** |  |  [optional]
**unconfirmedEmail** | **String** |  |  [optional]



