# OrganizationsApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteV5OrgsOrgMembershipsUsername**](OrganizationsApi.md#deleteV5OrgsOrgMembershipsUsername) | **DELETE** v5/orgs/{org}/memberships/{username} | 移除授权用户所管理组织中的成员
[**deleteV5UserMembershipsOrgsOrg**](OrganizationsApi.md#deleteV5UserMembershipsOrgsOrg) | **DELETE** v5/user/memberships/orgs/{org} | 退出一个组织
[**getV5OrgsOrg**](OrganizationsApi.md#getV5OrgsOrg) | **GET** v5/orgs/{org} | 获取一个组织
[**getV5OrgsOrgMembers**](OrganizationsApi.md#getV5OrgsOrgMembers) | **GET** v5/orgs/{org}/members | 列出一个组织的所有成员
[**getV5OrgsOrgMembershipsUsername**](OrganizationsApi.md#getV5OrgsOrgMembershipsUsername) | **GET** v5/orgs/{org}/memberships/{username} | 获取授权用户所属组织的一个成员
[**getV5UserMembershipsOrgs**](OrganizationsApi.md#getV5UserMembershipsOrgs) | **GET** v5/user/memberships/orgs | 列出授权用户在所属组织的成员资料
[**getV5UserMembershipsOrgsOrg**](OrganizationsApi.md#getV5UserMembershipsOrgsOrg) | **GET** v5/user/memberships/orgs/{org} | 获取授权用户在一个组织的成员资料
[**getV5UserOrgs**](OrganizationsApi.md#getV5UserOrgs) | **GET** v5/user/orgs | 列出授权用户所属的组织
[**getV5UsersUsernameOrgs**](OrganizationsApi.md#getV5UsersUsernameOrgs) | **GET** v5/users/{username}/orgs | 列出用户所属的组织
[**patchV5OrgsOrg**](OrganizationsApi.md#patchV5OrgsOrg) | **PATCH** v5/orgs/{org} | 更新授权用户所管理的组织资料
[**patchV5UserMembershipsOrgsOrg**](OrganizationsApi.md#patchV5UserMembershipsOrgsOrg) | **PATCH** v5/user/memberships/orgs/{org} | 更新授权用户在一个组织的成员资料
[**putV5OrgsOrgMembershipsUsername**](OrganizationsApi.md#putV5OrgsOrgMembershipsUsername) | **PUT** v5/orgs/{org}/memberships/{username} | 增加或更新授权用户所管理组织的成员


<a name="deleteV5OrgsOrgMembershipsUsername"></a>
# **deleteV5OrgsOrgMembershipsUsername**
> Void deleteV5OrgsOrgMembershipsUsername(org, username, accessToken)

移除授权用户所管理组织中的成员

移除授权用户所管理组织中的成员

### Example
```java
// Import classes:
//import com.gitee.api.api.OrganizationsApi;

OrganizationsApi apiInstance =  new ApiClient().create(OrganizationsApi.class);
String org = "org_example"; // String | 组织的路径(path/login)
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.deleteV5OrgsOrgMembershipsUsername(org, username, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **String**| 组织的路径(path/login) |
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteV5UserMembershipsOrgsOrg"></a>
# **deleteV5UserMembershipsOrgsOrg**
> Void deleteV5UserMembershipsOrgsOrg(org, accessToken)

退出一个组织

退出一个组织

### Example
```java
// Import classes:
//import com.gitee.api.api.OrganizationsApi;

OrganizationsApi apiInstance =  new ApiClient().create(OrganizationsApi.class);
String org = "org_example"; // String | 组织的路径(path/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.deleteV5UserMembershipsOrgsOrg(org, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **String**| 组织的路径(path/login) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5OrgsOrg"></a>
# **getV5OrgsOrg**
> Group getV5OrgsOrg(org, accessToken)

获取一个组织

获取一个组织

### Example
```java
// Import classes:
//import com.gitee.api.api.OrganizationsApi;

OrganizationsApi apiInstance =  new ApiClient().create(OrganizationsApi.class);
String org = "org_example"; // String | 组织的路径(path/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Group> result = apiInstance.getV5OrgsOrg(org, accessToken);
result.subscribe(new Observer<Group>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Group response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **String**| 组织的路径(path/login) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Group**](Group.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5OrgsOrgMembers"></a>
# **getV5OrgsOrgMembers**
> java.util.List&lt;UserBasic&gt; getV5OrgsOrgMembers(org, accessToken, page, perPage, role)

列出一个组织的所有成员

列出一个组织的所有成员

### Example
```java
// Import classes:
//import com.gitee.api.api.OrganizationsApi;

OrganizationsApi apiInstance =  new ApiClient().create(OrganizationsApi.class);
String org = "org_example"; // String | 组织的路径(path/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
String role = "all"; // String | 根据角色筛选成员
Observable<java.util.List<UserBasic>> result = apiInstance.getV5OrgsOrgMembers(org, accessToken, page, perPage, role);
result.subscribe(new Observer<java.util.List<UserBasic>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<UserBasic> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **String**| 组织的路径(path/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]
 **role** | **String**| 根据角色筛选成员 | [optional] [default to all] [enum: all, admin, member]

### Return type

[**java.util.List&lt;UserBasic&gt;**](UserBasic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5OrgsOrgMembershipsUsername"></a>
# **getV5OrgsOrgMembershipsUsername**
> GroupMember getV5OrgsOrgMembershipsUsername(org, username, accessToken)

获取授权用户所属组织的一个成员

获取授权用户所属组织的一个成员

### Example
```java
// Import classes:
//import com.gitee.api.api.OrganizationsApi;

OrganizationsApi apiInstance =  new ApiClient().create(OrganizationsApi.class);
String org = "org_example"; // String | 组织的路径(path/login)
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<GroupMember> result = apiInstance.getV5OrgsOrgMembershipsUsername(org, username, accessToken);
result.subscribe(new Observer<GroupMember>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(GroupMember response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **String**| 组织的路径(path/login) |
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**GroupMember**](GroupMember.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5UserMembershipsOrgs"></a>
# **getV5UserMembershipsOrgs**
> java.util.List&lt;GroupMember&gt; getV5UserMembershipsOrgs(accessToken, active, page, perPage)

列出授权用户在所属组织的成员资料

列出授权用户在所属组织的成员资料

### Example
```java
// Import classes:
//import com.gitee.api.api.OrganizationsApi;

OrganizationsApi apiInstance =  new ApiClient().create(OrganizationsApi.class);
String accessToken = "accessToken_example"; // String | 用户授权码
Boolean active = true; // Boolean | 根据成员是否已激活进行筛选资料，缺省返回所有资料
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<GroupMember>> result = apiInstance.getV5UserMembershipsOrgs(accessToken, active, page, perPage);
result.subscribe(new Observer<java.util.List<GroupMember>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<GroupMember> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **active** | **Boolean**| 根据成员是否已激活进行筛选资料，缺省返回所有资料 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;GroupMember&gt;**](GroupMember.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5UserMembershipsOrgsOrg"></a>
# **getV5UserMembershipsOrgsOrg**
> GroupMember getV5UserMembershipsOrgsOrg(org, accessToken)

获取授权用户在一个组织的成员资料

获取授权用户在一个组织的成员资料

### Example
```java
// Import classes:
//import com.gitee.api.api.OrganizationsApi;

OrganizationsApi apiInstance =  new ApiClient().create(OrganizationsApi.class);
String org = "org_example"; // String | 组织的路径(path/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<GroupMember> result = apiInstance.getV5UserMembershipsOrgsOrg(org, accessToken);
result.subscribe(new Observer<GroupMember>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(GroupMember response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **String**| 组织的路径(path/login) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**GroupMember**](GroupMember.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5UserOrgs"></a>
# **getV5UserOrgs**
> java.util.List&lt;Group&gt; getV5UserOrgs(accessToken, page, perPage, admin)

列出授权用户所属的组织

列出授权用户所属的组织

### Example
```java
// Import classes:
//import com.gitee.api.api.OrganizationsApi;

OrganizationsApi apiInstance =  new ApiClient().create(OrganizationsApi.class);
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Boolean admin = true; // Boolean | 只列出授权用户管理的组织
Observable<java.util.List<Group>> result = apiInstance.getV5UserOrgs(accessToken, page, perPage, admin);
result.subscribe(new Observer<java.util.List<Group>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<Group> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]
 **admin** | **Boolean**| 只列出授权用户管理的组织 | [optional]

### Return type

[**java.util.List&lt;Group&gt;**](Group.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5UsersUsernameOrgs"></a>
# **getV5UsersUsernameOrgs**
> java.util.List&lt;Group&gt; getV5UsersUsernameOrgs(username, accessToken, page, perPage)

列出用户所属的组织

列出用户所属的组织

### Example
```java
// Import classes:
//import com.gitee.api.api.OrganizationsApi;

OrganizationsApi apiInstance =  new ApiClient().create(OrganizationsApi.class);
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<Group>> result = apiInstance.getV5UsersUsernameOrgs(username, accessToken, page, perPage);
result.subscribe(new Observer<java.util.List<Group>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<Group> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;Group&gt;**](Group.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="patchV5OrgsOrg"></a>
# **patchV5OrgsOrg**
> GroupDetail patchV5OrgsOrg(org, accessToken, email, location, name, description, htmlUrl)

更新授权用户所管理的组织资料

更新授权用户所管理的组织资料

### Example
```java
// Import classes:
//import com.gitee.api.api.OrganizationsApi;

OrganizationsApi apiInstance =  new ApiClient().create(OrganizationsApi.class);
String org = "org_example"; // String | 组织的路径(path/login)
String accessToken = "accessToken_example"; // String | 用户授权码
String email = "email_example"; // String | 组织公开的邮箱地址
String location = "location_example"; // String | 组织所在地
String name = "name_example"; // String | 组织名称
String description = "description_example"; // String | 组织简介
String htmlUrl = "htmlUrl_example"; // String | 组织站点
Observable<GroupDetail> result = apiInstance.patchV5OrgsOrg(org, accessToken, email, location, name, description, htmlUrl);
result.subscribe(new Observer<GroupDetail>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(GroupDetail response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **String**| 组织的路径(path/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **email** | **String**| 组织公开的邮箱地址 | [optional]
 **location** | **String**| 组织所在地 | [optional]
 **name** | **String**| 组织名称 | [optional]
 **description** | **String**| 组织简介 | [optional]
 **htmlUrl** | **String**| 组织站点 | [optional]

### Return type

[**GroupDetail**](GroupDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="patchV5UserMembershipsOrgsOrg"></a>
# **patchV5UserMembershipsOrgsOrg**
> GroupMember patchV5UserMembershipsOrgsOrg(org, accessToken, remark)

更新授权用户在一个组织的成员资料

更新授权用户在一个组织的成员资料

### Example
```java
// Import classes:
//import com.gitee.api.api.OrganizationsApi;

OrganizationsApi apiInstance =  new ApiClient().create(OrganizationsApi.class);
String org = "org_example"; // String | 组织的路径(path/login)
String accessToken = "accessToken_example"; // String | 用户授权码
String remark = "remark_example"; // String | 在组织中的备注信息
Observable<GroupMember> result = apiInstance.patchV5UserMembershipsOrgsOrg(org, accessToken, remark);
result.subscribe(new Observer<GroupMember>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(GroupMember response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **String**| 组织的路径(path/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **remark** | **String**| 在组织中的备注信息 | [optional]

### Return type

[**GroupMember**](GroupMember.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="putV5OrgsOrgMembershipsUsername"></a>
# **putV5OrgsOrgMembershipsUsername**
> GroupMember putV5OrgsOrgMembershipsUsername(org, username, accessToken, role)

增加或更新授权用户所管理组织的成员

增加或更新授权用户所管理组织的成员

### Example
```java
// Import classes:
//import com.gitee.api.api.OrganizationsApi;

OrganizationsApi apiInstance =  new ApiClient().create(OrganizationsApi.class);
String org = "org_example"; // String | 组织的路径(path/login)
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
String role = "member"; // String | 设置用户在组织的角色
Observable<GroupMember> result = apiInstance.putV5OrgsOrgMembershipsUsername(org, username, accessToken, role);
result.subscribe(new Observer<GroupMember>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(GroupMember response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **String**| 组织的路径(path/login) |
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **role** | **String**| 设置用户在组织的角色 | [optional] [default to member] [enum: admin, member]

### Return type

[**GroupMember**](GroupMember.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

