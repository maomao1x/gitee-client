
# Issue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**url** | **String** |  |  [optional]
**repositoryUrl** | **String** |  |  [optional]
**labelsUrl** | **String** |  |  [optional]
**eventsUrl** | **String** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**number** | **String** |  |  [optional]
**state** | **String** |  |  [optional]
**title** | **String** |  |  [optional]
**body** | **String** |  |  [optional]
**user** | **String** |  |  [optional]
**labels** | [**Label**](Label.md) |  |  [optional]
**assignee** | **String** |  |  [optional]
**repository** | **String** |  |  [optional]
**milestone** | [**Milestone**](Milestone.md) |  |  [optional]
**createdAt** | [**java.util.Date**](java.util.Date.md) |  |  [optional]
**updatedAt** | [**java.util.Date**](java.util.Date.md) |  |  [optional]
**comments** | **Integer** |  |  [optional]



