# LabelsApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteV5ReposOwnerRepoIssuesNumberLabels**](LabelsApi.md#deleteV5ReposOwnerRepoIssuesNumberLabels) | **DELETE** v5/repos/{owner}/{repo}/issues/{number}/labels | 删除Issue所有标签
[**deleteV5ReposOwnerRepoIssuesNumberLabelsName**](LabelsApi.md#deleteV5ReposOwnerRepoIssuesNumberLabelsName) | **DELETE** v5/repos/{owner}/{repo}/issues/{number}/labels/{name} | 删除Issue标签
[**deleteV5ReposOwnerRepoLabelsName**](LabelsApi.md#deleteV5ReposOwnerRepoLabelsName) | **DELETE** v5/repos/{owner}/{repo}/labels/{name} | 删除一个项目标签
[**getV5ReposOwnerRepoIssuesNumberLabels**](LabelsApi.md#getV5ReposOwnerRepoIssuesNumberLabels) | **GET** v5/repos/{owner}/{repo}/issues/{number}/labels | 获取项目Issue的所有标签
[**getV5ReposOwnerRepoLabels**](LabelsApi.md#getV5ReposOwnerRepoLabels) | **GET** v5/repos/{owner}/{repo}/labels | 获取项目所有标签
[**getV5ReposOwnerRepoLabelsName**](LabelsApi.md#getV5ReposOwnerRepoLabelsName) | **GET** v5/repos/{owner}/{repo}/labels/{name} | 根据标签名称获取单个标签
[**patchV5ReposOwnerRepoLabelsOriginalName**](LabelsApi.md#patchV5ReposOwnerRepoLabelsOriginalName) | **PATCH** v5/repos/{owner}/{repo}/labels/{original_name} | 更新一个项目标签
[**postV5ReposOwnerRepoIssuesNumberLabels**](LabelsApi.md#postV5ReposOwnerRepoIssuesNumberLabels) | **POST** v5/repos/{owner}/{repo}/issues/{number}/labels | 创建Issue标签
[**postV5ReposOwnerRepoLabels**](LabelsApi.md#postV5ReposOwnerRepoLabels) | **POST** v5/repos/{owner}/{repo}/labels | 创建项目标签
[**putV5ReposOwnerRepoIssuesNumberLabels**](LabelsApi.md#putV5ReposOwnerRepoIssuesNumberLabels) | **PUT** v5/repos/{owner}/{repo}/issues/{number}/labels | 替换Issue所有标签


<a name="deleteV5ReposOwnerRepoIssuesNumberLabels"></a>
# **deleteV5ReposOwnerRepoIssuesNumberLabels**
> Void deleteV5ReposOwnerRepoIssuesNumberLabels(owner, repo, number, accessToken)

删除Issue所有标签

删除Issue所有标签

### Example
```java
// Import classes:
//import com.gitee.api.api.LabelsApi;

LabelsApi apiInstance =  new ApiClient().create(LabelsApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String number = "number_example"; // String | Issue 编号(区分大小写)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.deleteV5ReposOwnerRepoIssuesNumberLabels(owner, repo, number, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **number** | **String**| Issue 编号(区分大小写) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteV5ReposOwnerRepoIssuesNumberLabelsName"></a>
# **deleteV5ReposOwnerRepoIssuesNumberLabelsName**
> Void deleteV5ReposOwnerRepoIssuesNumberLabelsName(owner, repo, number, name, accessToken)

删除Issue标签

删除Issue标签

### Example
```java
// Import classes:
//import com.gitee.api.api.LabelsApi;

LabelsApi apiInstance =  new ApiClient().create(LabelsApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String number = "number_example"; // String | Issue 编号(区分大小写)
String name = "name_example"; // String | 标签名称
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.deleteV5ReposOwnerRepoIssuesNumberLabelsName(owner, repo, number, name, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **number** | **String**| Issue 编号(区分大小写) |
 **name** | **String**| 标签名称 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteV5ReposOwnerRepoLabelsName"></a>
# **deleteV5ReposOwnerRepoLabelsName**
> Void deleteV5ReposOwnerRepoLabelsName(owner, repo, name, accessToken)

删除一个项目标签

删除一个项目标签

### Example
```java
// Import classes:
//import com.gitee.api.api.LabelsApi;

LabelsApi apiInstance =  new ApiClient().create(LabelsApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String name = "name_example"; // String | 标签名称
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.deleteV5ReposOwnerRepoLabelsName(owner, repo, name, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **name** | **String**| 标签名称 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoIssuesNumberLabels"></a>
# **getV5ReposOwnerRepoIssuesNumberLabels**
> java.util.List&lt;Label&gt; getV5ReposOwnerRepoIssuesNumberLabels(owner, repo, number, accessToken)

获取项目Issue的所有标签

获取项目Issue的所有标签

### Example
```java
// Import classes:
//import com.gitee.api.api.LabelsApi;

LabelsApi apiInstance =  new ApiClient().create(LabelsApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String number = "number_example"; // String | Issue 编号(区分大小写)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<java.util.List<Label>> result = apiInstance.getV5ReposOwnerRepoIssuesNumberLabels(owner, repo, number, accessToken);
result.subscribe(new Observer<java.util.List<Label>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<Label> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **number** | **String**| Issue 编号(区分大小写) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**java.util.List&lt;Label&gt;**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoLabels"></a>
# **getV5ReposOwnerRepoLabels**
> java.util.List&lt;Label&gt; getV5ReposOwnerRepoLabels(owner, repo, accessToken)

获取项目所有标签

获取项目所有标签

### Example
```java
// Import classes:
//import com.gitee.api.api.LabelsApi;

LabelsApi apiInstance =  new ApiClient().create(LabelsApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<java.util.List<Label>> result = apiInstance.getV5ReposOwnerRepoLabels(owner, repo, accessToken);
result.subscribe(new Observer<java.util.List<Label>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<Label> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**java.util.List&lt;Label&gt;**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoLabelsName"></a>
# **getV5ReposOwnerRepoLabelsName**
> Label getV5ReposOwnerRepoLabelsName(owner, repo, name, accessToken)

根据标签名称获取单个标签

根据标签名称获取单个标签

### Example
```java
// Import classes:
//import com.gitee.api.api.LabelsApi;

LabelsApi apiInstance =  new ApiClient().create(LabelsApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String name = "name_example"; // String | 标签名称
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Label> result = apiInstance.getV5ReposOwnerRepoLabelsName(owner, repo, name, accessToken);
result.subscribe(new Observer<Label>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Label response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **name** | **String**| 标签名称 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Label**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="patchV5ReposOwnerRepoLabelsOriginalName"></a>
# **patchV5ReposOwnerRepoLabelsOriginalName**
> Label patchV5ReposOwnerRepoLabelsOriginalName(owner, repo, originalName, accessToken, name, color)

更新一个项目标签

更新一个项目标签

### Example
```java
// Import classes:
//import com.gitee.api.api.LabelsApi;

LabelsApi apiInstance =  new ApiClient().create(LabelsApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer originalName = 56; // Integer | 
String accessToken = "accessToken_example"; // String | 用户授权码
String name = "name_example"; // String | The name of a label
String color = "color_example"; // String | The color of a label
Observable<Label> result = apiInstance.patchV5ReposOwnerRepoLabelsOriginalName(owner, repo, originalName, accessToken, name, color);
result.subscribe(new Observer<Label>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Label response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **originalName** | **Integer**|  |
 **accessToken** | **String**| 用户授权码 | [optional]
 **name** | **String**| The name of a label | [optional]
 **color** | **String**| The color of a label | [optional]

### Return type

[**Label**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoIssuesNumberLabels"></a>
# **postV5ReposOwnerRepoIssuesNumberLabels**
> Label postV5ReposOwnerRepoIssuesNumberLabels(owner, repo, number, accessToken)

创建Issue标签

创建Issue标签  需要在请求的body里填上数组，元素为标签的名字。如: [\&quot;performance\&quot;, \&quot;bug\&quot;]

### Example
```java
// Import classes:
//import com.gitee.api.api.LabelsApi;

LabelsApi apiInstance =  new ApiClient().create(LabelsApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String number = "number_example"; // String | Issue 编号(区分大小写)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Label> result = apiInstance.postV5ReposOwnerRepoIssuesNumberLabels(owner, repo, number, accessToken);
result.subscribe(new Observer<Label>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Label response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **number** | **String**| Issue 编号(区分大小写) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Label**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoLabels"></a>
# **postV5ReposOwnerRepoLabels**
> Label postV5ReposOwnerRepoLabels(owner, repo, name, color, accessToken)

创建项目标签

创建项目标签

### Example
```java
// Import classes:
//import com.gitee.api.api.LabelsApi;

LabelsApi apiInstance =  new ApiClient().create(LabelsApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String name = "name_example"; // String | 标签名称
String color = "color_example"; // String | 标签颜色。为6位的数字，如: 000000
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Label> result = apiInstance.postV5ReposOwnerRepoLabels(owner, repo, name, color, accessToken);
result.subscribe(new Observer<Label>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Label response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **name** | **String**| 标签名称 |
 **color** | **String**| 标签颜色。为6位的数字，如: 000000 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Label**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putV5ReposOwnerRepoIssuesNumberLabels"></a>
# **putV5ReposOwnerRepoIssuesNumberLabels**
> Label putV5ReposOwnerRepoIssuesNumberLabels(owner, repo, number, accessToken)

替换Issue所有标签

替换Issue所有标签  需要在请求的body里填上数组，元素为标签的名字。如: [\&quot;performance\&quot;, \&quot;bug\&quot;]

### Example
```java
// Import classes:
//import com.gitee.api.api.LabelsApi;

LabelsApi apiInstance =  new ApiClient().create(LabelsApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String number = "number_example"; // String | Issue 编号(区分大小写)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Label> result = apiInstance.putV5ReposOwnerRepoIssuesNumberLabels(owner, repo, number, accessToken);
result.subscribe(new Observer<Label>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Label response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **number** | **String**| Issue 编号(区分大小写) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Label**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

