package com.gitee.api.api;

import com.gitee.api.ApiClient;
import com.gitee.api.model.SSHKey;
import com.gitee.api.model.SSHKeyBasic;
import com.gitee.api.model.User;
import com.gitee.api.model.UserAddress;
import com.gitee.api.model.UserBasic;
import com.gitee.api.model.UserDetail;
import com.gitee.api.model.UserEmail;
import org.junit.Before;
import org.junit.Test;


/**
 * API tests for UsersApi
 */
public class UsersApiTest {

    private UsersApi api;

    @Before
    public void setup() {
        api = new ApiClient().createService(UsersApi.class);
    }

    /**
     * 取消关注一个用户
     *
     * 取消关注一个用户
     */
    @Test
    public void deleteV5UserFollowingUsernameTest() {
        String username = null;
        String accessToken = null;
        // Void response = api.deleteV5UserFollowingUsername(username, accessToken);

        // TODO: test validations
    }
    /**
     * 删除一个公钥
     *
     * 删除一个公钥
     */
    @Test
    public void deleteV5UserKeysIdTest() {
        Integer id = null;
        String accessToken = null;
        // Void response = api.deleteV5UserKeysId(id, accessToken);

        // TODO: test validations
    }
    /**
     * 删除授权用户未激活的邮箱地址
     *
     * 删除授权用户未激活的邮箱地址
     */
    @Test
    public void deleteV5UserUnconfirmedEmailTest() {
        String accessToken = null;
        // Void response = api.deleteV5UserUnconfirmedEmail(accessToken);

        // TODO: test validations
    }
    /**
     * 获取授权用户的资料
     *
     * 获取授权用户的资料
     */
    @Test
    public void getV5UserTest() {
        String accessToken = null;
        // UserDetail response = api.getV5User(accessToken);

        // TODO: test validations
    }
    /**
     * 获取授权用户的地理信息
     *
     * 获取授权用户的地理信息
     */
    @Test
    public void getV5UserAddressTest() {
        String accessToken = null;
        // UserAddress response = api.getV5UserAddress(accessToken);

        // TODO: test validations
    }
    /**
     * 获取授权用户的邮箱地址
     *
     * 获取授权用户的邮箱地址
     */
    @Test
    public void getV5UserEmailsTest() {
        String accessToken = null;
        // UserEmail response = api.getV5UserEmails(accessToken);

        // TODO: test validations
    }
    /**
     * 列出授权用户的关注者
     *
     * 列出授权用户的关注者
     */
    @Test
    public void getV5UserFollowersTest() {
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<UserBasic> response = api.getV5UserFollowers(accessToken, page, perPage);

        // TODO: test validations
    }
    /**
     * 列出授权用户正关注的用户
     *
     * 列出授权用户正关注的用户
     */
    @Test
    public void getV5UserFollowingTest() {
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<UserBasic> response = api.getV5UserFollowing(accessToken, page, perPage);

        // TODO: test validations
    }
    /**
     * 检查授权用户是否关注了一个用户
     *
     * 检查授权用户是否关注了一个用户
     */
    @Test
    public void getV5UserFollowingUsernameTest() {
        String username = null;
        String accessToken = null;
        // Void response = api.getV5UserFollowingUsername(username, accessToken);

        // TODO: test validations
    }
    /**
     * 列出授权用户的所有公钥
     *
     * 列出授权用户的所有公钥
     */
    @Test
    public void getV5UserKeysTest() {
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<SSHKey> response = api.getV5UserKeys(accessToken, page, perPage);

        // TODO: test validations
    }
    /**
     * 获取一个公钥
     *
     * 获取一个公钥
     */
    @Test
    public void getV5UserKeysIdTest() {
        Integer id = null;
        String accessToken = null;
        // SSHKey response = api.getV5UserKeysId(id, accessToken);

        // TODO: test validations
    }
    /**
     * 获取一个用户
     *
     * 获取一个用户
     */
    @Test
    public void getV5UsersUsernameTest() {
        String username = null;
        String accessToken = null;
        // User response = api.getV5UsersUsername(username, accessToken);

        // TODO: test validations
    }
    /**
     * 列出指定用户的关注者
     *
     * 列出指定用户的关注者
     */
    @Test
    public void getV5UsersUsernameFollowersTest() {
        String username = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<UserBasic> response = api.getV5UsersUsernameFollowers(username, accessToken, page, perPage);

        // TODO: test validations
    }
    /**
     * 列出指定用户正在关注的用户
     *
     * 列出指定用户正在关注的用户
     */
    @Test
    public void getV5UsersUsernameFollowingTest() {
        String username = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<UserBasic> response = api.getV5UsersUsernameFollowing(username, accessToken, page, perPage);

        // TODO: test validations
    }
    /**
     * 检查指定用户是否关注目标用户
     *
     * 检查指定用户是否关注目标用户
     */
    @Test
    public void getV5UsersUsernameFollowingTargetUserTest() {
        String username = null;
        String targetUser = null;
        String accessToken = null;
        // Void response = api.getV5UsersUsernameFollowingTargetUser(username, targetUser, accessToken);

        // TODO: test validations
    }
    /**
     * 列出指定用户的所有公钥
     *
     * 列出指定用户的所有公钥
     */
    @Test
    public void getV5UsersUsernameKeysTest() {
        String username = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<SSHKeyBasic> response = api.getV5UsersUsernameKeys(username, accessToken, page, perPage);

        // TODO: test validations
    }
    /**
     * 更新授权用户的资料
     *
     * 更新授权用户的资料
     */
    @Test
    public void patchV5UserTest() {
        String accessToken = null;
        String name = null;
        String blog = null;
        String weibo = null;
        String bio = null;
        // UserDetail response = api.patchV5User(accessToken, name, blog, weibo, bio);

        // TODO: test validations
    }
    /**
     * 更新授权用户的地理信息
     *
     * 更新授权用户的地理信息
     */
    @Test
    public void patchV5UserAddressTest() {
        String accessToken = null;
        String name = null;
        String tel = null;
        String address = null;
        String province = null;
        String city = null;
        String zipCode = null;
        String comment = null;
        // UserDetail response = api.patchV5UserAddress(accessToken, name, tel, address, province, city, zipCode, comment);

        // TODO: test validations
    }
    /**
     * 添加授权用户的新邮箱地址
     *
     * 添加授权用户的新邮箱地址
     */
    @Test
    public void postV5UserEmailsTest() {
        String email = null;
        String accessToken = null;
        // UserEmail response = api.postV5UserEmails(email, accessToken);

        // TODO: test validations
    }
    /**
     * 添加一个公钥
     *
     * 添加一个公钥
     */
    @Test
    public void postV5UserKeysTest() {
        String key = null;
        String title = null;
        String accessToken = null;
        // SSHKey response = api.postV5UserKeys(key, title, accessToken);

        // TODO: test validations
    }
    /**
     * 关注一个用户
     *
     * 关注一个用户
     */
    @Test
    public void putV5UserFollowingUsernameTest() {
        String username = null;
        String accessToken = null;
        // Void response = api.putV5UserFollowingUsername(username, accessToken);

        // TODO: test validations
    }
}
