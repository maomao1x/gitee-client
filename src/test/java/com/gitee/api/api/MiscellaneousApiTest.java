package com.gitee.api.api;

import com.gitee.api.ApiClient;
import org.junit.Before;
import org.junit.Test;


/**
 * API tests for MiscellaneousApi
 */
public class MiscellaneousApiTest {

    private MiscellaneousApi api;

    @Before
    public void setup() {
        api = new ApiClient().createService(MiscellaneousApi.class);
    }

    /**
     * 列出可使用的 Emoji
     *
     * 列出可使用的 Emoji
     */
    @Test
    public void getV5EmojisTest() {
        String accessToken = null;
        // Void response = api.getV5Emojis(accessToken);

        // TODO: test validations
    }
    /**
     * 列出可使用的 .gitignore 模板
     *
     * 列出可使用的 .gitignore 模板
     */
    @Test
    public void getV5GitignoreTemplatesTest() {
        String accessToken = null;
        // Void response = api.getV5GitignoreTemplates(accessToken);

        // TODO: test validations
    }
    /**
     * 获取一个 .gitignore 模板
     *
     * 获取一个 .gitignore 模板
     */
    @Test
    public void getV5GitignoreTemplatesNameTest() {
        String name = null;
        String accessToken = null;
        // Void response = api.getV5GitignoreTemplatesName(name, accessToken);

        // TODO: test validations
    }
    /**
     * 获取一个 .gitignore 模板原始文件
     *
     * 获取一个 .gitignore 模板原始文件
     */
    @Test
    public void getV5GitignoreTemplatesNameRawTest() {
        String name = null;
        String accessToken = null;
        // Void response = api.getV5GitignoreTemplatesNameRaw(name, accessToken);

        // TODO: test validations
    }
    /**
     * 列出可使用的开源许可协议
     *
     * 列出可使用的开源许可协议
     */
    @Test
    public void getV5LicensesTest() {
        String accessToken = null;
        // Void response = api.getV5Licenses(accessToken);

        // TODO: test validations
    }
    /**
     * 获取一个开源许可协议
     *
     * 获取一个开源许可协议
     */
    @Test
    public void getV5LicensesLicenseTest() {
        String license = null;
        String accessToken = null;
        // Void response = api.getV5LicensesLicense(license, accessToken);

        // TODO: test validations
    }
    /**
     * 获取一个开源许可协议原始文件
     *
     * 获取一个开源许可协议原始文件
     */
    @Test
    public void getV5LicensesLicenseRawTest() {
        String license = null;
        String accessToken = null;
        // Void response = api.getV5LicensesLicenseRaw(license, accessToken);

        // TODO: test validations
    }
    /**
     * 获取一个项目使用的开源许可协议
     *
     * 获取一个项目使用的开源许可协议
     */
    @Test
    public void getV5ReposOwnerRepoLicenseTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        // Void response = api.getV5ReposOwnerRepoLicense(owner, repo, accessToken);

        // TODO: test validations
    }
    /**
     * 渲染 Markdown 文本
     *
     * 渲染 Markdown 文本
     */
    @Test
    public void postV5MarkdownTest() {
        String text = null;
        String accessToken = null;
        // Void response = api.postV5Markdown(text, accessToken);

        // TODO: test validations
    }
}
