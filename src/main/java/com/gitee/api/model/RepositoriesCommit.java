package com.gitee.api.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * this is description
 */
public class RepositoriesCommit {
    @SerializedName("url")
    private String url = null;

    @SerializedName("sha")
    private String sha = null;

    @SerializedName("html_url")
    private String htmlUrl = null;

    @SerializedName("comments_url")
    private String commentsUrl = null;

    @SerializedName("commit")
    private RepositoriesCommitCommit commit = null;

    @SerializedName("author")
    private RepositoriesCommitAuthor author = null;

    @SerializedName("committer")
    private RepositoriesCommitAuthor committer = null;

    @SerializedName("parents")

    private java.util.List<RepositoriesCommitParents> parents = null;

    @SerializedName("stats")
    private RepositoriesCommitStats stats = null;

    public RepositoriesCommit url(String url) {
        this.url = url;
        return this;
    }

    /**
     * Get url
     *
     * @return url
     **/
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public RepositoriesCommit sha(String sha) {
        this.sha = sha;
        return this;
    }

    /**
     * Get sha
     *
     * @return sha
     **/
    public String getSha() {
        return sha;
    }

    public void setSha(String sha) {
        this.sha = sha;
    }

    public RepositoriesCommit htmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
        return this;
    }

    /**
     * Get htmlUrl
     *
     * @return htmlUrl
     **/
    public String getHtmlUrl() {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    public RepositoriesCommit commentsUrl(String commentsUrl) {
        this.commentsUrl = commentsUrl;
        return this;
    }

    /**
     * Get commentsUrl
     *
     * @return commentsUrl
     **/
    public String getCommentsUrl() {
        return commentsUrl;
    }

    public void setCommentsUrl(String commentsUrl) {
        this.commentsUrl = commentsUrl;
    }

    public RepositoriesCommit commit(RepositoriesCommitCommit commit) {
        this.commit = commit;
        return this;
    }

    /**
     * Get commit
     *
     * @return commit
     **/
    @ApiModelProperty(value = "")
    public RepositoriesCommitCommit getCommit() {
        return commit;
    }

    public void setCommit(RepositoriesCommitCommit commit) {
        this.commit = commit;
    }

    public RepositoriesCommit author(RepositoriesCommitAuthor author) {
        this.author = author;
        return this;
    }

    /**
     * Get author
     *
     * @return author
     **/
    @ApiModelProperty(value = "")
    public RepositoriesCommitAuthor getAuthor() {
        return author;
    }

    public void setAuthor(RepositoriesCommitAuthor author) {
        this.author = author;
    }

    public RepositoriesCommit committer(RepositoriesCommitAuthor committer) {
        this.committer = committer;
        return this;
    }

    /**
     * Get committer
     *
     * @return committer
     **/
    @ApiModelProperty(value = "")
    public RepositoriesCommitAuthor getCommitter() {
        return committer;
    }

    public void setCommitter(RepositoriesCommitAuthor committer) {
        this.committer = committer;
    }

    public RepositoriesCommit parents(java.util.List<RepositoriesCommitParents> parents) {
        this.parents = parents;
        return this;
    }

    public RepositoriesCommit addParentsItem(RepositoriesCommitParents parentsItem) {
        if (this.parents == null) {
            this.parents = new java.util.ArrayList<RepositoriesCommitParents>();
        }
        this.parents.add(parentsItem);
        return this;
    }

    /**
     * Get parents
     *
     * @return parents
     **/
    @ApiModelProperty(value = "")
    public java.util.List<RepositoriesCommitParents> getParents() {
        return parents;
    }

    public void setParents(java.util.List<RepositoriesCommitParents> parents) {
        this.parents = parents;
    }

    public RepositoriesCommit stats(RepositoriesCommitStats stats) {
        this.stats = stats;
        return this;
    }

    /**
     * Get stats
     *
     * @return stats
     **/
    @ApiModelProperty(value = "")
    public RepositoriesCommitStats getStats() {
        return stats;
    }

    public void setStats(RepositoriesCommitStats stats) {
        this.stats = stats;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RepositoriesCommit repositoriesCommit = (RepositoriesCommit) o;
        return Objects.equals(this.url, repositoriesCommit.url) &&
                Objects.equals(this.sha, repositoriesCommit.sha) &&
                Objects.equals(this.htmlUrl, repositoriesCommit.htmlUrl) &&
                Objects.equals(this.commentsUrl, repositoriesCommit.commentsUrl) &&
                Objects.equals(this.commit, repositoriesCommit.commit) &&
                Objects.equals(this.author, repositoriesCommit.author) &&
                Objects.equals(this.committer, repositoriesCommit.committer) &&
                Objects.equals(this.parents, repositoriesCommit.parents) &&
                Objects.equals(this.stats, repositoriesCommit.stats);
    }

    @Override
    public int hashCode() {
        return Objects.hash(url, sha, htmlUrl, commentsUrl, commit, author, committer, parents, stats);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class RepositoriesCommit {\n");

        sb.append("    url: ").append(toIndentedString(url)).append("\n");
        sb.append("    sha: ").append(toIndentedString(sha)).append("\n");
        sb.append("    htmlUrl: ").append(toIndentedString(htmlUrl)).append("\n");
        sb.append("    commentsUrl: ").append(toIndentedString(commentsUrl)).append("\n");
        sb.append("    commit: ").append(toIndentedString(commit)).append("\n");
        sb.append("    author: ").append(toIndentedString(author)).append("\n");
        sb.append("    committer: ").append(toIndentedString(committer)).append("\n");
        sb.append("    parents: ").append(toIndentedString(parents)).append("\n");
        sb.append("    stats: ").append(toIndentedString(stats)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

