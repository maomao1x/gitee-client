package com.gitee.api.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * CommitContentCommitAuthor
 */
public class CommitContentCommitAuthor {
    @SerializedName("name")
    private String name = null;

    @SerializedName("date")
    private java.util.Date date = null;

    @SerializedName("email")
    private String email = null;

    public CommitContentCommitAuthor name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Get name
     *
     * @return name
     **/
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CommitContentCommitAuthor date(java.util.Date date) {
        this.date = date;
        return this;
    }

    /**
     * Get date
     *
     * @return date
     **/
    public java.util.Date getDate() {
        return date;
    }

    public void setDate(java.util.Date date) {
        this.date = date;
    }

    public CommitContentCommitAuthor email(String email) {
        this.email = email;
        return this;
    }

    /**
     * Get email
     *
     * @return email
     **/
    @ApiModelProperty(example = "191287278@qq.com", value = "")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CommitContentCommitAuthor commitContentCommitAuthor = (CommitContentCommitAuthor) o;
        return Objects.equals(this.name, commitContentCommitAuthor.name) &&
                Objects.equals(this.date, commitContentCommitAuthor.date) &&
                Objects.equals(this.email, commitContentCommitAuthor.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, date, email);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class CommitContentCommitAuthor {\n");

        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    date: ").append(toIndentedString(date)).append("\n");
        sb.append("    email: ").append(toIndentedString(email)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

