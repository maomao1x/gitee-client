package com.gitee.api.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * IssueCommentTarget
 */

@javax.annotation.Generated(value = "io.swagger.generator.online.JavaClientCodegenImpl", date = "2017-11-16T13:19:19.951+08:00")

public class IssueCommentTarget   {
  @SerializedName("issue")
  private IssueCommentTargetIssue issue = null;

  public IssueCommentTarget issue(IssueCommentTargetIssue issue) {
    this.issue = issue;
    return this;
  }

   /**
   * Get issue
   * @return issue
  **/
  @ApiModelProperty(value = "")
  public IssueCommentTargetIssue getIssue() {
    return issue;
  }

  public void setIssue(IssueCommentTargetIssue issue) {
    this.issue = issue;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    IssueCommentTarget issueCommentTarget = (IssueCommentTarget) o;
    return Objects.equals(this.issue, issueCommentTarget.issue);
  }

  @Override
  public int hashCode() {
    return Objects.hash(issue);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class IssueCommentTarget {\n");
    
    sb.append("    issue: ").append(toIndentedString(issue)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

