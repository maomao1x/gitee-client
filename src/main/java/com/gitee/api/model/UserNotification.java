package com.gitee.api.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * this is description
 */
public class UserNotification   {
  @SerializedName("id")
  private Integer id = null;

  @SerializedName("repository")
  private UserNotificationRepository repository = null;

  @SerializedName("unread")
  private Boolean unread = null;

  @SerializedName("mute")
  private Boolean mute = null;

  @SerializedName("subject")
  private UserNotificationSubject subject = null;

  @SerializedName("updated_at")
  private java.util.Date updatedAt = null;

  @SerializedName("url")
  private String url = null;

  public UserNotification id(Integer id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(example = "14063784", value = "")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public UserNotification repository(UserNotificationRepository repository) {
    this.repository = repository;
    return this;
  }

   /**
   * Get repository
   * @return repository
  **/
  @ApiModelProperty(value = "")
  public UserNotificationRepository getRepository() {
    return repository;
  }

  public void setRepository(UserNotificationRepository repository) {
    this.repository = repository;
  }

  public UserNotification unread(Boolean unread) {
    this.unread = unread;
    return this;
  }

   /**
   * Get unread
   * @return unread
  **/
  @ApiModelProperty(value = "")
  public Boolean isUnread() {
    return unread;
  }

  public void setUnread(Boolean unread) {
    this.unread = unread;
  }

  public UserNotification mute(Boolean mute) {
    this.mute = mute;
    return this;
  }

   /**
   * Get mute
   * @return mute
  **/
  @ApiModelProperty(value = "")
  public Boolean isMute() {
    return mute;
  }

  public void setMute(Boolean mute) {
    this.mute = mute;
  }

  public UserNotification subject(UserNotificationSubject subject) {
    this.subject = subject;
    return this;
  }

   /**
   * Get subject
   * @return subject
  **/
  @ApiModelProperty(value = "")
  public UserNotificationSubject getSubject() {
    return subject;
  }

  public void setSubject(UserNotificationSubject subject) {
    this.subject = subject;
  }

  public UserNotification updatedAt(java.util.Date updatedAt) {
    this.updatedAt = updatedAt;
    return this;
  }

   /**
   * Get updatedAt
   * @return updatedAt
  **/
  @ApiModelProperty(example = "2017-11-15T18:39:01+08:00", value = "")
  public java.util.Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(java.util.Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public UserNotification url(String url) {
    this.url = url;
    return this;
  }

   /**
   * Get url
   * @return url
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/notification/threads/14063784", value = "")
  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserNotification userNotification = (UserNotification) o;
    return Objects.equals(this.id, userNotification.id) &&
        Objects.equals(this.repository, userNotification.repository) &&
        Objects.equals(this.unread, userNotification.unread) &&
        Objects.equals(this.mute, userNotification.mute) &&
        Objects.equals(this.subject, userNotification.subject) &&
        Objects.equals(this.updatedAt, userNotification.updatedAt) &&
        Objects.equals(this.url, userNotification.url);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, repository, unread, mute, subject, updatedAt, url);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserNotification {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    repository: ").append(toIndentedString(repository)).append("\n");
    sb.append("    unread: ").append(toIndentedString(unread)).append("\n");
    sb.append("    mute: ").append(toIndentedString(mute)).append("\n");
    sb.append("    subject: ").append(toIndentedString(subject)).append("\n");
    sb.append("    updatedAt: ").append(toIndentedString(updatedAt)).append("\n");
    sb.append("    url: ").append(toIndentedString(url)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

