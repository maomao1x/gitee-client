package com.gitee.api.model;

/**
 * created by wuyu on 2017/11/16
 */
public class Gitignore {

    private String name;

    private String source;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
