package com.gitee.api.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * this is description
 */
@ApiModel(description = "this is description")


public class BranchUser   {
  @SerializedName("login")
  private String login = null;

  @SerializedName("id")
  private Integer id = null;

  @SerializedName("avatar_url")
  private String avatarUrl = null;

  @SerializedName("url")
  private String url = null;

  @SerializedName("html_url")
  private String htmlUrl = null;

  @SerializedName("followers_url")
  private String followersUrl = null;

  @SerializedName("following_url")
  private String followingUrl = null;

  @SerializedName("gists_url")
  private String gistsUrl = null;

  @SerializedName("starred_url")
  private String starredUrl = null;

  @SerializedName("subscriptions_url")
  private String subscriptionsUrl = null;

  @SerializedName("organizations_url")
  private String organizationsUrl = null;

  @SerializedName("repos_url")
  private String reposUrl = null;

  @SerializedName("events_url")
  private String eventsUrl = null;

  @SerializedName("received_events_url")
  private String receivedEventsUrl = null;

  @SerializedName("type")
  private String type = null;

  @SerializedName("site_admin")
  private Boolean siteAdmin = null;

  @SerializedName("permissions")
  private BranchUserPermissions permissions = null;

  public BranchUser login(String login) {
    this.login = login;
    return this;
  }

   /**
   * Get login
   * @return login
  **/
  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public BranchUser id(Integer id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public BranchUser avatarUrl(String avatarUrl) {
    this.avatarUrl = avatarUrl;
    return this;
  }

   /**
   * Get avatarUrl
   * @return avatarUrl
  **/
  public String getAvatarUrl() {
    return avatarUrl;
  }

  public void setAvatarUrl(String avatarUrl) {
    this.avatarUrl = avatarUrl;
  }

  public BranchUser url(String url) {
    this.url = url;
    return this;
  }

   /**
   * Get url
   * @return url
  **/
  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public BranchUser htmlUrl(String htmlUrl) {
    this.htmlUrl = htmlUrl;
    return this;
  }

   /**
   * Get htmlUrl
   * @return htmlUrl
  **/
  public String getHtmlUrl() {
    return htmlUrl;
  }

  public void setHtmlUrl(String htmlUrl) {
    this.htmlUrl = htmlUrl;
  }

  public BranchUser followersUrl(String followersUrl) {
    this.followersUrl = followersUrl;
    return this;
  }

   /**
   * Get followersUrl
   * @return followersUrl
  **/
  public String getFollowersUrl() {
    return followersUrl;
  }

  public void setFollowersUrl(String followersUrl) {
    this.followersUrl = followersUrl;
  }

  public BranchUser followingUrl(String followingUrl) {
    this.followingUrl = followingUrl;
    return this;
  }

   /**
   * Get followingUrl
   * @return followingUrl
  **/
  public String getFollowingUrl() {
    return followingUrl;
  }

  public void setFollowingUrl(String followingUrl) {
    this.followingUrl = followingUrl;
  }

  public BranchUser gistsUrl(String gistsUrl) {
    this.gistsUrl = gistsUrl;
    return this;
  }

   /**
   * Get gistsUrl
   * @return gistsUrl
  **/
  public String getGistsUrl() {
    return gistsUrl;
  }

  public void setGistsUrl(String gistsUrl) {
    this.gistsUrl = gistsUrl;
  }

  public BranchUser starredUrl(String starredUrl) {
    this.starredUrl = starredUrl;
    return this;
  }

   /**
   * Get starredUrl
   * @return starredUrl
  **/
  public String getStarredUrl() {
    return starredUrl;
  }

  public void setStarredUrl(String starredUrl) {
    this.starredUrl = starredUrl;
  }

  public BranchUser subscriptionsUrl(String subscriptionsUrl) {
    this.subscriptionsUrl = subscriptionsUrl;
    return this;
  }

   /**
   * Get subscriptionsUrl
   * @return subscriptionsUrl
  **/
  public String getSubscriptionsUrl() {
    return subscriptionsUrl;
  }

  public void setSubscriptionsUrl(String subscriptionsUrl) {
    this.subscriptionsUrl = subscriptionsUrl;
  }

  public BranchUser organizationsUrl(String organizationsUrl) {
    this.organizationsUrl = organizationsUrl;
    return this;
  }

   /**
   * Get organizationsUrl
   * @return organizationsUrl
  **/
  public String getOrganizationsUrl() {
    return organizationsUrl;
  }

  public void setOrganizationsUrl(String organizationsUrl) {
    this.organizationsUrl = organizationsUrl;
  }

  public BranchUser reposUrl(String reposUrl) {
    this.reposUrl = reposUrl;
    return this;
  }

   /**
   * Get reposUrl
   * @return reposUrl
  **/
  public String getReposUrl() {
    return reposUrl;
  }

  public void setReposUrl(String reposUrl) {
    this.reposUrl = reposUrl;
  }

  public BranchUser eventsUrl(String eventsUrl) {
    this.eventsUrl = eventsUrl;
    return this;
  }

   /**
   * Get eventsUrl
   * @return eventsUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/users/kmg/events{/privacy}", value = "")
  public String getEventsUrl() {
    return eventsUrl;
  }

  public void setEventsUrl(String eventsUrl) {
    this.eventsUrl = eventsUrl;
  }

  public BranchUser receivedEventsUrl(String receivedEventsUrl) {
    this.receivedEventsUrl = receivedEventsUrl;
    return this;
  }

   /**
   * Get receivedEventsUrl
   * @return receivedEventsUrl
  **/
  public String getReceivedEventsUrl() {
    return receivedEventsUrl;
  }

  public void setReceivedEventsUrl(String receivedEventsUrl) {
    this.receivedEventsUrl = receivedEventsUrl;
  }

  public BranchUser type(String type) {
    this.type = type;
    return this;
  }

   /**
   * Get type
   * @return type
  **/
  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public BranchUser siteAdmin(Boolean siteAdmin) {
    this.siteAdmin = siteAdmin;
    return this;
  }

   /**
   * Get siteAdmin
   * @return siteAdmin
  **/
  public Boolean isSiteAdmin() {
    return siteAdmin;
  }

  public void setSiteAdmin(Boolean siteAdmin) {
    this.siteAdmin = siteAdmin;
  }

  public BranchUser permissions(BranchUserPermissions permissions) {
    this.permissions = permissions;
    return this;
  }

   /**
   * Get permissions
   * @return permissions
  **/
  @ApiModelProperty(value = "")
  public BranchUserPermissions getPermissions() {
    return permissions;
  }

  public void setPermissions(BranchUserPermissions permissions) {
    this.permissions = permissions;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BranchUser branchUser = (BranchUser) o;
    return Objects.equals(this.login, branchUser.login) &&
        Objects.equals(this.id, branchUser.id) &&
        Objects.equals(this.avatarUrl, branchUser.avatarUrl) &&
        Objects.equals(this.url, branchUser.url) &&
        Objects.equals(this.htmlUrl, branchUser.htmlUrl) &&
        Objects.equals(this.followersUrl, branchUser.followersUrl) &&
        Objects.equals(this.followingUrl, branchUser.followingUrl) &&
        Objects.equals(this.gistsUrl, branchUser.gistsUrl) &&
        Objects.equals(this.starredUrl, branchUser.starredUrl) &&
        Objects.equals(this.subscriptionsUrl, branchUser.subscriptionsUrl) &&
        Objects.equals(this.organizationsUrl, branchUser.organizationsUrl) &&
        Objects.equals(this.reposUrl, branchUser.reposUrl) &&
        Objects.equals(this.eventsUrl, branchUser.eventsUrl) &&
        Objects.equals(this.receivedEventsUrl, branchUser.receivedEventsUrl) &&
        Objects.equals(this.type, branchUser.type) &&
        Objects.equals(this.siteAdmin, branchUser.siteAdmin) &&
        Objects.equals(this.permissions, branchUser.permissions);
  }

  @Override
  public int hashCode() {
    return Objects.hash(login, id, avatarUrl, url, htmlUrl, followersUrl, followingUrl, gistsUrl, starredUrl, subscriptionsUrl, organizationsUrl, reposUrl, eventsUrl, receivedEventsUrl, type, siteAdmin, permissions);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BranchUser {\n");
    
    sb.append("    login: ").append(toIndentedString(login)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    avatarUrl: ").append(toIndentedString(avatarUrl)).append("\n");
    sb.append("    url: ").append(toIndentedString(url)).append("\n");
    sb.append("    htmlUrl: ").append(toIndentedString(htmlUrl)).append("\n");
    sb.append("    followersUrl: ").append(toIndentedString(followersUrl)).append("\n");
    sb.append("    followingUrl: ").append(toIndentedString(followingUrl)).append("\n");
    sb.append("    gistsUrl: ").append(toIndentedString(gistsUrl)).append("\n");
    sb.append("    starredUrl: ").append(toIndentedString(starredUrl)).append("\n");
    sb.append("    subscriptionsUrl: ").append(toIndentedString(subscriptionsUrl)).append("\n");
    sb.append("    organizationsUrl: ").append(toIndentedString(organizationsUrl)).append("\n");
    sb.append("    reposUrl: ").append(toIndentedString(reposUrl)).append("\n");
    sb.append("    eventsUrl: ").append(toIndentedString(eventsUrl)).append("\n");
    sb.append("    receivedEventsUrl: ").append(toIndentedString(receivedEventsUrl)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    siteAdmin: ").append(toIndentedString(siteAdmin)).append("\n");
    sb.append("    permissions: ").append(toIndentedString(permissions)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

