package com.gitee.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

/**
 * EventPayload
 */
public class EventPayload   {
  @SerializedName("ref")
  private String ref = null;

  @SerializedName("before")
  private String before = null;

  @SerializedName("after")
  private String after = null;

  @SerializedName("created")
  private Boolean created = null;

  @SerializedName("deleted")
  private Boolean deleted = null;

  @SerializedName("size")
  private Integer size = null;

  @SerializedName("commits")
  
  private java.util.List<EventPayloadCommits> commits = null;

  public EventPayload ref(String ref) {
    this.ref = ref;
    return this;
  }

   /**
   * Get ref
   * @return ref
  **/
  public String getRef() {
    return ref;
  }

  public void setRef(String ref) {
    this.ref = ref;
  }

  public EventPayload before(String before) {
    this.before = before;
    return this;
  }

   /**
   * Get before
   * @return before
  **/
  public String getBefore() {
    return before;
  }

  public void setBefore(String before) {
    this.before = before;
  }

  public EventPayload after(String after) {
    this.after = after;
    return this;
  }

   /**
   * Get after
   * @return after
  **/
  public String getAfter() {
    return after;
  }

  public void setAfter(String after) {
    this.after = after;
  }

  public EventPayload created(Boolean created) {
    this.created = created;
    return this;
  }

   /**
   * Get created
   * @return created
  **/
  public Boolean isCreated() {
    return created;
  }

  public void setCreated(Boolean created) {
    this.created = created;
  }

  public EventPayload deleted(Boolean deleted) {
    this.deleted = deleted;
    return this;
  }

   /**
   * Get deleted
   * @return deleted
  **/
  public Boolean isDeleted() {
    return deleted;
  }

  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }

  public EventPayload size(Integer size) {
    this.size = size;
    return this;
  }

   /**
   * Get size
   * @return size
  **/
  public Integer getSize() {
    return size;
  }

  public void setSize(Integer size) {
    this.size = size;
  }

  public EventPayload commits(java.util.List<EventPayloadCommits> commits) {
    this.commits = commits;
    return this;
  }

  public EventPayload addCommitsItem(EventPayloadCommits commitsItem) {
    if (this.commits == null) {
      this.commits = new java.util.ArrayList<EventPayloadCommits>();
    }
    this.commits.add(commitsItem);
    return this;
  }

   /**
   * Get commits
   * @return commits
  **/
  public java.util.List<EventPayloadCommits> getCommits() {
    return commits;
  }

  public void setCommits(java.util.List<EventPayloadCommits> commits) {
    this.commits = commits;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EventPayload eventPayload = (EventPayload) o;
    return Objects.equals(this.ref, eventPayload.ref) &&
        Objects.equals(this.before, eventPayload.before) &&
        Objects.equals(this.after, eventPayload.after) &&
        Objects.equals(this.created, eventPayload.created) &&
        Objects.equals(this.deleted, eventPayload.deleted) &&
        Objects.equals(this.size, eventPayload.size) &&
        Objects.equals(this.commits, eventPayload.commits);
  }

  @Override
  public int hashCode() {
    return Objects.hash(ref, before, after, created, deleted, size, commits);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class EventPayload {\n");
    
    sb.append("    ref: ").append(toIndentedString(ref)).append("\n");
    sb.append("    before: ").append(toIndentedString(before)).append("\n");
    sb.append("    after: ").append(toIndentedString(after)).append("\n");
    sb.append("    created: ").append(toIndentedString(created)).append("\n");
    sb.append("    deleted: ").append(toIndentedString(deleted)).append("\n");
    sb.append("    size: ").append(toIndentedString(size)).append("\n");
    sb.append("    commits: ").append(toIndentedString(commits)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

