package com.gitee.api.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * this is description
 */
public class UserMessage   {
  @SerializedName("id")
  private Integer id = null;

  @SerializedName("sender")
  private UserMessageSender sender = null;

  @SerializedName("unread")
  private Boolean unread = null;

  @SerializedName("content")
  private String content = null;

  @SerializedName("updated_at")
  private java.util.Date updatedAt = null;

  @SerializedName("url")
  private String url = null;

  public UserMessage id(Integer id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public UserMessage sender(UserMessageSender sender) {
    this.sender = sender;
    return this;
  }

   /**
   * Get sender
   * @return sender
  **/
  @ApiModelProperty(value = "")
  public UserMessageSender getSender() {
    return sender;
  }

  public void setSender(UserMessageSender sender) {
    this.sender = sender;
  }

  public UserMessage unread(Boolean unread) {
    this.unread = unread;
    return this;
  }

   /**
   * Get unread
   * @return unread
  **/
  @ApiModelProperty(value = "")
  public Boolean isUnread() {
    return unread;
  }

  public void setUnread(Boolean unread) {
    this.unread = unread;
  }

  public UserMessage content(String content) {
    this.content = content;
    return this;
  }

   /**
   * Get content
   * @return content
  **/
  @ApiModelProperty(example = "", value = "")
  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public UserMessage updatedAt(java.util.Date updatedAt) {
    this.updatedAt = updatedAt;
    return this;
  }

   /**
   * Get updatedAt
   * @return updatedAt
  **/
  public java.util.Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(java.util.Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public UserMessage url(String url) {
    this.url = url;
    return this;
  }

   /**
   * Get url
   * @return url
  **/
  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserMessage userMessage = (UserMessage) o;
    return Objects.equals(this.id, userMessage.id) &&
        Objects.equals(this.sender, userMessage.sender) &&
        Objects.equals(this.unread, userMessage.unread) &&
        Objects.equals(this.content, userMessage.content) &&
        Objects.equals(this.updatedAt, userMessage.updatedAt) &&
        Objects.equals(this.url, userMessage.url);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, sender, unread, content, updatedAt, url);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserMessage {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    sender: ").append(toIndentedString(sender)).append("\n");
    sb.append("    unread: ").append(toIndentedString(unread)).append("\n");
    sb.append("    content: ").append(toIndentedString(content)).append("\n");
    sb.append("    updatedAt: ").append(toIndentedString(updatedAt)).append("\n");
    sb.append("    url: ").append(toIndentedString(url)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

