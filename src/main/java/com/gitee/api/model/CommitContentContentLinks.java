package com.gitee.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

/**
 * CommitContentContentLinks
 */
public class CommitContentContentLinks {
    @SerializedName("self")
    private String self = null;

    @SerializedName("html")
    private String html = null;

    public CommitContentContentLinks self(String self) {
        this.self = self;
        return this;
    }

    /**
     * Get self
     *
     * @return self
     **/
    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public CommitContentContentLinks html(String html) {
        this.html = html;
        return this;
    }

    /**
     * Get html
     *
     * @return html
     **/
    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CommitContentContentLinks commitContentContentLinks = (CommitContentContentLinks) o;
        return Objects.equals(this.self, commitContentContentLinks.self) &&
                Objects.equals(this.html, commitContentContentLinks.html);
    }

    @Override
    public int hashCode() {
        return Objects.hash(self, html);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class CommitContentContentLinks {\n");

        sb.append("    self: ").append(toIndentedString(self)).append("\n");
        sb.append("    html: ").append(toIndentedString(html)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

