package com.gitee.api.api;

import com.gitee.api.model.Gitignore;
import com.gitee.api.model.License;
import retrofit2.http.GET;
import retrofit2.http.POST;
import rx.Observable;

import java.util.List;


public interface MiscellaneousApi {
    /**
     * 列出可使用的 Emoji
     * 列出可使用的 Emoji
     *
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Void&gt;
     */
    @GET("v5/emojis")
    Observable<Void> getV5Emojis(
            @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 列出可使用的 .gitignore 模板
     * 列出可使用的 .gitignore 模板
     *
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Void&gt;
     */
    @GET("v5/gitignore/templates")
    Observable<List<String>> getV5GitignoreTemplates(
            @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 获取一个 .gitignore 模板
     * 获取一个 .gitignore 模板
     *
     * @param name        .gitignore 模板名 (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Void&gt;
     */
    @GET("v5/gitignore/templates/{name}")
    Observable<Gitignore> getV5GitignoreTemplatesName(
            @retrofit2.http.Path("name") String name, @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 获取一个 .gitignore 模板原始文件
     * 获取一个 .gitignore 模板原始文件
     *
     * @param name        .gitignore 模板名 (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Void&gt;
     */
    @GET("v5/gitignore/templates/{name}/raw")
    Observable<String> getV5GitignoreTemplatesNameRaw(
            @retrofit2.http.Path("name") String name, @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 列出可使用的开源许可协议
     * 列出可使用的开源许可协议
     *
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Void&gt;
     */
    @GET("v5/licenses")
    Observable<List<String>> getV5Licenses(
            @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 获取一个开源许可协议
     * 获取一个开源许可协议
     *
     * @param license     协议名称 (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Void&gt;
     */
    @GET("v5/licenses/{license}")
    Observable<License> getV5LicensesLicense(
            @retrofit2.http.Path("license") String license, @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 获取一个开源许可协议原始文件
     * 获取一个开源许可协议原始文件
     *
     * @param license     协议名称 (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Void&gt;
     */
    @GET("v5/licenses/{license}/raw")
    Observable<String> getV5LicensesLicenseRaw(
            @retrofit2.http.Path("license") String license, @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 获取一个项目使用的开源许可协议
     * 获取一个项目使用的开源许可协议
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Void&gt;
     */
    @GET("v5/repos/{owner}/{repo}/license")
    Observable<String> getV5ReposOwnerRepoLicense(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 渲染 Markdown 文本
     * 渲染 Markdown 文本
     *
     * @param text        Markdown 文本 (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Void&gt;
     */
    @retrofit2.http.FormUrlEncoded
    @POST("v5/markdown")
    Observable<String> postV5Markdown(
            @retrofit2.http.Field("text") String text, @retrofit2.http.Field("access_token") String accessToken
    );

}
