package com.gitee.api.api;

import com.gitee.api.CollectionFormats.*;

import rx.Observable;
import retrofit2.http.*;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;

import com.gitee.api.model.PullRequest;
import com.gitee.api.model.PullRequestComments;
import com.gitee.api.model.PullRequestCommits;
import com.gitee.api.model.PullRequestFiles;
import com.gitee.api.model.UserBasic;


public interface PullRequestsApi {
  /**
   * 删除评论
   * 删除评论
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param id 评论的ID (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @DELETE("v5/repos/{owner}/{repo}/pulls/comments/{id}")
  Observable<Void> deleteV5ReposOwnerRepoPullsCommentsId(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("id") Integer id, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 移除审查人员
   * 移除审查人员
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param number 第几个PR，即本项目PR的序数 (required)
   * @param reviewers 审核人员的username，数组形式: [\&quot;oschina\&quot;, \&quot;tom\&quot;], 此处试验直接对username换行即可 (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @DELETE("v5/repos/{owner}/{repo}/pulls/{number}/requested_reviewers")
  Observable<Void> deleteV5ReposOwnerRepoPullsNumberRequestedReviewers(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("number") Integer number, @retrofit2.http.Field("reviewers") java.util.List<String> reviewers, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 获取Pull Request列表
   * 获取Pull Request列表
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param accessToken 用户授权码 (optional)
   * @param state 可选。Pull Request 状态 (optional, default to open)
   * @param head 可选。Pull Request 提交的源分支。格式：branch 或者：username:branch (optional)
   * @param base 可选。Pull Request 提交目标分支的名称。 (optional)
   * @param sort 可选。排序字段，默认按创建时间 (optional, default to created)
   * @param direction 可选。升序/降序 (optional, default to desc)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;PullRequest&gt;&gt;
   */
  @GET("v5/repos/{owner}/{repo}/pulls")
  Observable<java.util.List<PullRequest>> getV5ReposOwnerRepoPulls(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("state") String state, @retrofit2.http.Query("head") String head, @retrofit2.http.Query("base") String base, @retrofit2.http.Query("sort") String sort, @retrofit2.http.Query("direction") String direction, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 获取该项目下的所有Pull Request评论
   * 获取该项目下的所有Pull Request评论
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param accessToken 用户授权码 (optional)
   * @param sort 可选。按创建时间/更新时间排序 (optional, default to created)
   * @param direction 可选。升序/降序 (optional, default to desc)
   * @param since 起始的更新时间，要求时间格式为 ISO 8601 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;PullRequestComments&gt;&gt;
   */
  @GET("v5/repos/{owner}/{repo}/pulls/comments")
  Observable<java.util.List<PullRequestComments>> getV5ReposOwnerRepoPullsComments(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("sort") String sort, @retrofit2.http.Query("direction") String direction, @retrofit2.http.Query("since") String since, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 获取Pull Request的某个评论
   * 获取Pull Request的某个评论
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param id  (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;PullRequestComments&gt;
   */
  @GET("v5/repos/{owner}/{repo}/pulls/comments/{id}")
  Observable<PullRequestComments> getV5ReposOwnerRepoPullsCommentsId(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("id") Integer id, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 获取单个Pull Request
   * 获取单个Pull Request
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param number 第几个PR，即本项目PR的序数 (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;PullRequest&gt;
   */
  @GET("v5/repos/{owner}/{repo}/pulls/{number}")
  Observable<PullRequest> getV5ReposOwnerRepoPullsNumber(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("number") Integer number, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 获取某个Pull Request的所有评论
   * 获取某个Pull Request的所有评论
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param number 第几个PR，即本项目PR的序数 (required)
   * @param accessToken 用户授权码 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;PullRequestComments&gt;&gt;
   */
  @GET("v5/repos/{owner}/{repo}/pulls/{number}/comments")
  Observable<java.util.List<PullRequestComments>> getV5ReposOwnerRepoPullsNumberComments(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("number") Integer number, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 获取某Pull Request的所有Commit信息。最多显示250条Commit
   * 获取某Pull Request的所有Commit信息。最多显示250条Commit
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param number 第几个PR，即本项目PR的序数 (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;java.util.List&lt;PullRequestCommits&gt;&gt;
   */
  @GET("v5/repos/{owner}/{repo}/pulls/{number}/commits")
  Observable<java.util.List<PullRequestCommits>> getV5ReposOwnerRepoPullsNumberCommits(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("number") Integer number, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * Pull Request Commit文件列表。最多显示300条diff
   * Pull Request Commit文件列表。最多显示300条diff
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param number 第几个PR，即本项目PR的序数 (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;java.util.List&lt;PullRequestFiles&gt;&gt;
   */
  @GET("v5/repos/{owner}/{repo}/pulls/{number}/files")
  Observable<java.util.List<PullRequestFiles>> getV5ReposOwnerRepoPullsNumberFiles(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("number") Integer number, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 判断Pull Request是否已经合并
   * 判断Pull Request是否已经合并
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param number 第几个PR，即本项目PR的序数 (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @GET("v5/repos/{owner}/{repo}/pulls/{number}/merge")
  Observable<Void> getV5ReposOwnerRepoPullsNumberMerge(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("number") Integer number, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 获取审查人员的列表
   * 获取审查人员的列表
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param number 第几个PR，即本项目PR的序数 (required)
   * @param accessToken 用户授权码 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;UserBasic&gt;
   */
  @GET("v5/repos/{owner}/{repo}/pulls/{number}/requested_reviewers")
  Observable<UserBasic> getV5ReposOwnerRepoPullsNumberRequestedReviewers(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("number") Integer number, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 编辑评论
   * 编辑评论
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param id 评论的ID (required)
   * @param body 必填。评论内容 (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;PullRequestComments&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @PATCH("v5/repos/{owner}/{repo}/pulls/comments/{id}")
  Observable<PullRequestComments> patchV5ReposOwnerRepoPullsCommentsId(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("id") Integer id, @retrofit2.http.Field("body") String body, @retrofit2.http.Field("access_token") String accessToken
  );

  /**
   * 更新Pull Request信息
   * 更新Pull Request信息
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param number 第几个PR，即本项目PR的序数 (required)
   * @param accessToken 用户授权码 (optional)
   * @param title 可选。Pull Request 标题 (optional)
   * @param body 可选。Pull Request 内容 (optional)
   * @param state 可选。Pull Request 状态 (optional)
   * @return Call&lt;PullRequest&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @PATCH("v5/repos/{owner}/{repo}/pulls/{number}")
  Observable<PullRequest> patchV5ReposOwnerRepoPullsNumber(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("number") Integer number, @retrofit2.http.Field("access_token") String accessToken, @retrofit2.http.Field("title") String title, @retrofit2.http.Field("body") String body, @retrofit2.http.Field("state") String state
  );

  /**
   * 创建Pull Request
   * 创建Pull Request
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param title 必填。Pull Request 标题 (required)
   * @param head 必填。Pull Request 提交的源分支。格式：branch 或者：username:branch (required)
   * @param base 必填。Pull Request 提交目标分支的名称 (required)
   * @param accessToken 用户授权码 (optional)
   * @param body 可选。Pull Request 内容 (optional)
   * @param issue 可选。Pull Request的标题和内容可以根据指定的Issue Id自动填充 (optional)
   * @return Call&lt;PullRequest&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @POST("v5/repos/{owner}/{repo}/pulls")
  Observable<PullRequest> postV5ReposOwnerRepoPulls(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Field("title") String title, @retrofit2.http.Field("head") String head, @retrofit2.http.Field("base") String base, @retrofit2.http.Field("access_token") String accessToken, @retrofit2.http.Field("body") String body, @retrofit2.http.Field("issue") String issue
  );

  /**
   * 提交Pull Request评论
   * 提交Pull Request评论
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param number 第几个PR，即本项目PR的序数 (required)
   * @param body 必填。评论内容 (required)
   * @param accessToken 用户授权码 (optional)
   * @param commitId 可选。PR代码评论的commit id (optional)
   * @param path 可选。PR代码评论的文件名 (optional)
   * @param position 可选。PR代码评论diff中的行数 (optional)
   * @return Call&lt;PullRequestComments&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @POST("v5/repos/{owner}/{repo}/pulls/{number}/comments")
  Observable<PullRequestComments> postV5ReposOwnerRepoPullsNumberComments(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("number") Integer number, @retrofit2.http.Field("body") String body, @retrofit2.http.Field("access_token") String accessToken, @retrofit2.http.Field("commit_id") String commitId, @retrofit2.http.Field("path") String path, @retrofit2.http.Field("position") Integer position
  );

  /**
   * 增加审查人员
   * 增加审查人员
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param number 第几个PR，即本项目PR的序数 (required)
   * @param reviewers 审核人员的username，数组形式: [\&quot;oschina\&quot;, \&quot;tom\&quot;], 此处试验直接对标签名换行即可 (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;PullRequest&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @POST("v5/repos/{owner}/{repo}/pulls/{number}/requested_reviewers")
  Observable<PullRequest> postV5ReposOwnerRepoPullsNumberRequestedReviewers(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("number") Integer number, @retrofit2.http.Field("reviewers") java.util.List<String> reviewers, @retrofit2.http.Field("access_token") String accessToken
  );

  /**
   * 合并Pull Request
   * 合并Pull Request
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param number 第几个PR，即本项目PR的序数 (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @PUT("v5/repos/{owner}/{repo}/pulls/{number}/merge")
  Observable<Void> putV5ReposOwnerRepoPullsNumberMerge(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("number") Integer number, @retrofit2.http.Field("access_token") String accessToken
  );

}
